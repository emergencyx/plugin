// Copyright (C) 2012-2017 Promotion Software GmbH


// This cpp-file creates the optional precompiled header, all other cpp-files are just using it


//[-------------------------------------------------------]
//[ Includes                                              ]
//[-------------------------------------------------------]
#include "game_plugin/PrecompiledHeader.h"
