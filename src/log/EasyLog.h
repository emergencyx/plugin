
#include <qsf/log/LogSystem.h>
/*
#define STRINGIZE_LVL2(x) #x
#define STRINGIZE(x) STRINGIZE_LVL2(x)

#define BUILDSTRING(x) __FILE__##STRINGIZE(x)

#define PRINT(lvl, msg) do{                                                                                          \
    if (qsf::Qsf::instance() && qsf::Qsf::instance()->getLogSystem().shouldBeLogged(qsf::LogMessage::severityLevel)) \
    {                                                                                                                \
      const qsf::LogMessage::SeverityLevel severityLevelReal = qsf::LogMessage::SeverityLevel::severityLevel;        \
        qsf::Qsf::instance()->getLogSystem().printf(severityLevelReal, text, ##__VA_ARGS__);                         \
    }                                                                                                                \
}while(0)
*/

//#define PRINTF(lvl, msg, ...) do{QSF_LOG_PRINTF(lvl, msg, __VA_ARGS__)}while(0);
