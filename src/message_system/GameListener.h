
#pragma once

#include <qsf/message/MessageProxy.h>

namespace emx
{
  class MainMenuListener;

  class GameListener
  {
  public:
    GameListener();
    ~GameListener();

    void init();

  private:
    void onStartup(const qsf::MessageParameters& parameters);
    void onShutdown(const qsf::MessageParameters& parameters);
    void onMenuShow(const qsf::MessageParameters& parameters);
    void onMenuHide(const qsf::MessageParameters& parameters);

    qsf::MessageProxy c_mainMenuShowMsgProxy;
    qsf::MessageProxy c_mainMenuHideMsgProxy;
    qsf::MessageProxy c_startGameMsgProxy;
    qsf::MessageProxy c_shutdownGameMsgProxy;

    MainMenuListener* c_mainMenuListener;
  };
} // emx