
#include "game_plugin/PrecompiledHeader.h"
#include "../src/message_system/GameListener.h"

#include "../src/main_menu/MainMenuListener.h"

#include <em5/EM5Helper.h>
#include <em5/plugin/Messages.h>

#include <qsf/log/LogSystem.h>

#include <boost/bind.hpp>

#include <iostream>

namespace emx
{
  GameListener::GameListener() : 
    c_mainMenuListener(nullptr)
  {

  }

  GameListener::~GameListener()
  {
    c_mainMenuShowMsgProxy.unregister();
    c_mainMenuHideMsgProxy.unregister();
    c_startGameMsgProxy.unregister();
    c_shutdownGameMsgProxy.unregister();

    MainMenuListener::destroySingleton();
  }

  void GameListener::init()
  {
    c_mainMenuShowMsgProxy.registerAt(qsf::MessageConfiguration(em5::Messages::GAME_STARTUP_FINISHED), boost::bind(&GameListener::onStartup, this, _1));
    //c_mainMenuHideMsgProxy.registerAt(qsf::MessageConfiguration(em5::Messages::GAME_SHUTDOWN_STARTING), boost::bind(&GameListener::onShutdown, this, _1));
    c_startGameMsgProxy.registerAt(qsf::MessageConfiguration(em5::Messages::MAIN_MENU_SHOW), boost::bind(&GameListener::onMenuShow, this, _1));
    c_shutdownGameMsgProxy.registerAt(qsf::MessageConfiguration(em5::Messages::MAIN_MENU_HIDE), boost::bind(&GameListener::onMenuHide, this, _1));
  }

  void GameListener::onStartup(const qsf::MessageParameters& parameters)
  {
    std::cout << "Hello, World!" << std::endl << "This is onStartup()" << std::endl;
  }
  
  void GameListener::onShutdown(const qsf::MessageParameters& parameters)
  {
    std::cout << "Hello, World!" << std::endl << "This is onShutdown()" << std::endl;
  }

  void GameListener::onMenuShow(const qsf::MessageParameters& parameters)
  {
    MainMenuListener::singleton();
  }

  void GameListener::onMenuHide(const qsf::MessageParameters& parameters)
  {
    MainMenuListener::destroySingleton();
  }


  /*
  GuiListener* c_mainMenuListenerInstance;
  */


} // emx
