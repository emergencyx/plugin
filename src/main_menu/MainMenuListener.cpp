
#include "game_plugin/PrecompiledHeader.h"
#include "../src/main_menu/MainMenuListener.h"

#include "../src/main_menu/MainMenuEMX.h"

#include <em5/EM5Helper.h>
#include <em5/gui/EmergencyGui.h>

#include <qsf/log/LogSystem.h>
#include <qsf/gui/GuiContext.h>

#include <Rocket/Core/Context.h>

namespace emx
{
  int MainMenuListener::numOfInstances = 0;
  MainMenuListener* MainMenuListener::instance = nullptr;

  MainMenuListener* MainMenuListener::singleton()
  {
    if (instance == nullptr)
      instance = new MainMenuListener();
    return instance;
  }

  void MainMenuListener::destroySingleton()
  {
    if (instance != nullptr)
    {
      delete instance;
      instance = nullptr;
    }
    else
    {
      QSF_LOG_PRINT(WARNING, "EMX | MainMenuListener: failed attempt to delete the MainMenuListener-singleton.");
    }
  }

  MainMenuListener::MainMenuListener() :
    mainMenuInstance(nullptr)
  {
    EM5_GUI.getGuiContext().getRocketCoreContext().AddEventListener("click", this);
    numOfInstances++;
  }

  MainMenuListener::~MainMenuListener()
  {
    EM5_GUI.getGuiContext().getRocketCoreContext().RemoveEventListener("click", this);
    numOfInstances--;
  }

  void MainMenuListener::ProcessEvent(Rocket::Core::Event& event)
  {
    QSF_LOG_PRINT(ERROR, "wir sind drinne!");
    QSF_LOG_PRINTF(ERROR, "EventTyp: %s", event.GetType().CString());
    QSF_LOG_PRINTF(ERROR, "Element-ID: %s", event.GetTargetElement()->GetId().CString());
    QSF_LOG_PRINTF(ERROR, "Element-ID-L�nge: %d", event.GetTargetElement()->GetId().Length());

    if (event.GetTargetElement()->GetId() == "show_emx")
    {
      mainMenuInstance = event.GetTargetElement()->GetOwnerDocument();
      mainMenuInstance->Hide();
      MainMenuEMX::createEmxGui();
    }
    else if (event.GetTargetElement()->GetId() == "hide_emx")
    {
      MainMenuEMX::destroyEmxGui();
      if (mainMenuInstance != nullptr)
        mainMenuInstance->Show();
      else
        QSF_LOG_PRINT(ERROR, "Could not make the main menu visible due to a lost pointer.");
    }
  }
}
