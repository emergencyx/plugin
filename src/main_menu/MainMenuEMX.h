
#pragma once

#include <qsf/gui/GuiDocument.h>

#include <Rocket/Core/EventListener.h>

namespace emx
{
  class MainMenuEMX : public qsf::GuiDocument
  {
  public:

    static void createEmxGui();
    static void destroyEmxGui();

    void processEvent(const std::string& eventKey, Rocket::Core::Event& event) override;
    
    bool onPreShow() override
    {
      return true;
    }

    void onPostHide() override
    {}

  private:
    static MainMenuEMX* instance;
    explicit MainMenuEMX();
  };
}