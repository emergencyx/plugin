
#include "game_plugin/PrecompiledHeader.h"
#include "../src/main_menu/MainMenuEMX.h"

#include "../src/log/EasyLog.h"

#include <em5/EM5Helper.h>          // EM5_GUI
#include <em5/gui/EmergencyGui.h>   // getGuiContext()



#include <Rocket/Core/Event.h>

namespace emx
{
  MainMenuEMX* MainMenuEMX::instance = nullptr;

  MainMenuEMX::MainMenuEMX()
    : GuiDocument(EM5_GUI.getGuiContext())
  {
    loadByFilename("em5/gui/mainmenu/mainmenu_emx.rml");
  }

  void MainMenuEMX::createEmxGui()
  {
    if (instance == nullptr)
    {
      instance = new MainMenuEMX();
    }
    else
    {
      QSF_LOG_PRINT(WARNING, "MainMenuEMX: emxGUI already/still (?) existing! Destroying old and creating a new instance now...");

      instance->hide();
      delete instance;
      instance = new MainMenuEMX();

      QSF_LOG_PRINT(WARNING, "MainMenuEMX: new instance created. In the log-file there should now be two messages by MainMenuEMX in a row. If not, an error occured!");
      // todo: �ber LogSystem auf letzte Nachricht zugreifen -> wenn nicht von MainMenuEMX dann ERROR!
    }

    instance->show();
  }

  void MainMenuEMX::destroyEmxGui()
  {
    if (instance != nullptr)
    {
      delete instance;
      instance = nullptr;
    }
    else
    {
      QSF_LOG_PRINT(WARNING, "MainMenuEMX: emxGUI already destroyed when destroyEmxGui() was called!");
    }

  }

  void MainMenuEMX::processEvent(const std::string& eventKey, Rocket::Core::Event& event)
  {
    QSF_LOG_PRINTF(ERROR, "MainMenuEMX: not a real Error... just an eventkey: %s", eventKey);
    // todo: replace with useful stuff...
  }
}