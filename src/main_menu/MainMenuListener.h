
#pragma once

#include <Rocket/Core/EventListener.h>
#include <Rocket/Core/ElementDocument.h>

namespace emx
{
  class MainMenuListener : public Rocket::Core::EventListener
  {
  public:
    static MainMenuListener* singleton();
    static void destroySingleton();

  private:
    MainMenuListener();
    ~MainMenuListener();

    static int numOfInstances;
    static MainMenuListener* instance;

    Rocket::Core::ElementDocument* mainMenuInstance;

    void ProcessEvent(Rocket::Core::Event& event) override;
  };
}