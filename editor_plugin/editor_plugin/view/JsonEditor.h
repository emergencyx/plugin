// Copyright (C) 2012-2017 Promotion Software GmbH


//[-------------------------------------------------------]
//[ Header guard                                          ]
//[-------------------------------------------------------]
#pragma once


//[-------------------------------------------------------]
//[ Includes                                              ]
//[-------------------------------------------------------]
#include <qsf_editor/view/View.h>

#include <QtCore/qobject.h>
#include <QtWidgets/qtablewidget.h>

#include <camp/userobject.hpp>

#include <boost/property_tree/ptree.hpp>


//[-------------------------------------------------------]
//[ Forward declarations                                  ]
//[-------------------------------------------------------]
namespace Ui
{
  class JsonEditor;
}
namespace qsf
{
  namespace editor
  {
    namespace base
    {
      class Operation;
    }
  }
}


//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
namespace emx
{
  namespace editor
  {


    //[-------------------------------------------------------]
    //[ Classes                                               ]
    //[-------------------------------------------------------]
    /**
    *  @brief
    *    Indicator view class
    *
    *  @note
    *    - The UI is created via source code
    */
    class JsonEditor : public qsf::editor::View
    {


      //[-------------------------------------------------------]
      //[ Qt definitions (MOC)                                  ]
      //[-------------------------------------------------------]
      Q_OBJECT	// All files using the Q_OBJECT macro need to be compiled using the Meta-Object Compiler (MOC) of Qt, else slots won't work!
                // (VisualStudio: Header file -> Right click -> Properties -> "Custom Build Tool")


                //[-------------------------------------------------------]
                //[ Public definitions                                    ]
                //[-------------------------------------------------------]
    public:
      static const uint32 PLUGINABLE_ID;	///< "user::editor::JsonEditor" unique pluginable view ID


                                          //[-------------------------------------------------------]
                                          //[ Public methods                                        ]
                                          //[-------------------------------------------------------]
    public:
      /**
      *  @brief
      *    Constructor
      *
      *  @param[in] viewManager
      *    Optional pointer to the view manager this view should be registered to, can be a null pointer
      *  @param[in] qWidgetParent
      *    Pointer to parent Qt widget, can be a null pointer (in this case you're responsible for destroying this view instance)
      */
      JsonEditor(qsf::editor::ViewManager* viewManager, QWidget* qWidgetParent);

      /**
      *  @brief
      *    Destructor
      */
      virtual ~JsonEditor();


      //[-------------------------------------------------------]
      //[ Protected virtual qsf::editor::View methods           ]
      //[-------------------------------------------------------]
    protected:
      virtual void retranslateUi() override;
      virtual void changeVisibility(bool visible) override;


      //[-------------------------------------------------------]
      //[ Protected virtual QWidget methods                     ]
      //[-------------------------------------------------------]
    protected:
      virtual void showEvent(QShowEvent* qShowEvent) override;
      virtual void hideEvent(QHideEvent* qHideEvent) override;


      //[-------------------------------------------------------]
      //[ Private methods                                       ]
      //[-------------------------------------------------------]
    private:
      void clearTable();
      void blockCellChange(bool block);
      // todo: remove debug function
      void printPropertyTree(boost::property_tree::ptree& tree);


      //[-------------------------------------------------------]
      //[ Private Qt slots (MOC)                                ]
      //[-------------------------------------------------------]
      private Q_SLOTS:
      void onPushButtonOpen(const bool pressed);
      void onPushButtonSave(const bool pressed);
      void onPushButtonSubmit(const bool pressed);
      void onCurrentIndexChangedFilePicker(int index);
      void onCurrentIndexChangedUnitPool(int index);
      void onUnitPoolTableContentChanged(int row, int column);
//      void onUnitPoolTableSelectionChanged();
//      void onUnitPoolTableItemActivated(QTableWidgetItem* item);
      void onUnitPoolTableCurrentItemChanged(QTableWidgetItem* current, QTableWidgetItem* previous);
      // qsf::editor::OperationManager
      void onUndoOperationExecuted(const qsf::editor::base::Operation& operation);
      void onRedoOperationExecuted(const qsf::editor::base::Operation& operation);


      //[-------------------------------------------------------]
      //[ Private data                                          ]
      //[-------------------------------------------------------]
    private:
      Ui::JsonEditor*	mUiJsonEditor;	///< UI view instance, can be a null pointer, we have to destroy the instance in case we no longer need it
      QString currentCellContent;                     // when switching the table item in focus, save new item's content in order to enable key-replacement
      boost::property_tree::ptree parsedJson;         // whole file which is being worked on
      boost::property_tree::ptree currentUnitPool;    // only the property tree of the actual UnitPool

      //[-------------------------------------------------------]
      //[ CAMP reflection system                                ]
      //[-------------------------------------------------------]
      QSF_CAMP_RTTI()	// Only adds the virtual method "campClassId()", nothing more


    };


    //[-------------------------------------------------------]
    //[ Namespace                                             ]
    //[-------------------------------------------------------]
  } // editor
} // user


  //[-------------------------------------------------------]
  //[ CAMP reflection system                                ]
  //[-------------------------------------------------------]
QSF_CAMP_TYPE_NONCOPYABLE(emx::editor::JsonEditor)
