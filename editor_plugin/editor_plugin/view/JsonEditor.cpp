// Copyright (C) 2012-2017 Promotion Software GmbH


//[-------------------------------------------------------]
//[ Includes                                              ]
//[-------------------------------------------------------]
#include "editor_plugin/PrecompiledHeader.h"
//#include "../src/view/JsonEditor.h"
#include "editor_plugin/view/JsonEditor.h"
#include "ui_JsonEditor.h" // Automatically created by Qt's uic (output directory is "tmp\qt\uic\qsf_editor" within the hand configured Visual Studio files, another directory when using CMake)

#include <qsf/log/LogSystem.h>

#include <qsf/file/FileHelper.h>

#include <qsf_editor/operation/utility/RebuildGuiOperation.h>
#include <qsf_editor/application/manager/CameraManager.h>
#include <qsf_editor/EditorHelper.h>

#include <qsf_editor_base/operation/entity/CreateEntityOperation.h>
#include <qsf_editor_base/operation/entity/DestroyEntityOperation.h>
#include <qsf_editor_base/operation/component/CreateComponentOperation.h>
#include <qsf_editor_base/operation/component/DestroyComponentOperation.h>
#include <qsf_editor_base/operation/component/SetComponentPropertyOperation.h>

#include <qsf/map/Map.h>
#include <qsf/map/Entity.h>
#include <qsf/selection/EntitySelectionManager.h>
#include <qsf/QsfHelper.h>

#include <QtWidgets/qfiledialog.h>
//#include <QtCore/qtranslator.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/nowide/fstream.hpp>


//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
namespace emx
{
  namespace editor
  {

    //[-------------------------------------------------------]
    //[ Public definitions                                    ]
    //[-------------------------------------------------------]
    const uint32 JsonEditor::PLUGINABLE_ID = qsf::StringHash("qsf::editor::JsonEditor");


    //[-------------------------------------------------------]
    //[ Public methods                                        ]
    //[-------------------------------------------------------]
    JsonEditor::JsonEditor(qsf::editor::ViewManager* viewManager, QWidget* qWidgetParent) :
      View(viewManager, qWidgetParent),
      mUiJsonEditor(nullptr),
      currentCellContent()
    {
      // Add the created Qt dock widget to the given Qt main window and tabify it for better usability
      addViewAndTabify(reinterpret_cast<QMainWindow&>(*qWidgetParent), Qt::RightDockWidgetArea);
    }

    JsonEditor::~JsonEditor()
    { 
      // Destroy the UI view instance
      if (nullptr != mUiJsonEditor)
      {
        delete mUiJsonEditor;
      }
    }


    //[-------------------------------------------------------]
    //[ Protected virtual qsf::editor::View methods           ]
    //[-------------------------------------------------------]
    void JsonEditor::retranslateUi()
    {
      // Retranslate the content of the UI, this automatically clears the previous content
      mUiJsonEditor->retranslateUi(this);
    }

    void JsonEditor::changeVisibility(bool visible)
    {
      // Lazy evaluation: If the view is shown the first time, create its content
      if (visible && nullptr == mUiJsonEditor)
      {
        // Setup the view content
        QWidget* contentWidget = new QWidget(this);
        {
          // Load content to widget
          mUiJsonEditor = new Ui::JsonEditor();
          mUiJsonEditor->setupUi(contentWidget);
        }

        // Set content to view
        setWidget(contentWidget);

        // Connect Qt signals/slots
        connect(mUiJsonEditor->pushButtonOpen, SIGNAL(clicked(bool)), this, SLOT(onPushButtonOpen(bool)));
        connect(mUiJsonEditor->pushButtonSave, SIGNAL(clicked(bool)), this, SLOT(onPushButtonSave(bool)));
        connect(mUiJsonEditor->pushButtonSubmit, SIGNAL(clicked(bool)), this, SLOT(onPushButtonSubmit(bool)));
        connect(mUiJsonEditor->dropdownFilePicker, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChangedFilePicker(int)));
        connect(mUiJsonEditor->dropdownUnitPool, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChangedUnitPool(int)));
        connect(mUiJsonEditor->tableExistingUnitPool, SIGNAL(cellChanged(int, int)), this, SLOT(onUnitPoolTableContentChanged(int, int)));
        connect(mUiJsonEditor->tableExistingUnitPool, SIGNAL(itemSelectionChanged()), this, SLOT(onUnitPoolTableSelectionChanged()));
        connect(mUiJsonEditor->tableExistingUnitPool, SIGNAL(itemActivated(QTableWidgetItem*)), this, SLOT(onUnitPoolTableItemActivated(QTableWidgetItem*)));
        connect(mUiJsonEditor->tableExistingUnitPool, SIGNAL(currentItemChanged(QTableWidgetItem *, QTableWidgetItem *)), this, SLOT(onUnitPoolTableCurrentItemChanged(QTableWidgetItem*, QTableWidgetItem*)));
      }
    }


    //[-------------------------------------------------------]
    //[ Private methods                                       ]
    //[-------------------------------------------------------]
    void JsonEditor::clearTable()
    {
      blockCellChange(true);
      // remove all entries from table
      for (int i = mUiJsonEditor->tableExistingUnitPool->rowCount(); i >= 0; --i)
      {
        mUiJsonEditor->tableExistingUnitPool->removeRow(i);
      }
      blockCellChange(false);
    }

    void JsonEditor::blockCellChange(bool block)
    {
      if(block)
        disconnect(mUiJsonEditor->tableExistingUnitPool, SIGNAL(cellChanged(int, int)), this, SLOT(onUnitPoolTableContentChanged(int, int)));
      else
        connect(mUiJsonEditor->tableExistingUnitPool, SIGNAL(cellChanged(int, int)), this, SLOT(onUnitPoolTableContentChanged(int, int)));
    }

    void JsonEditor::printPropertyTree(boost::property_tree::ptree& tree)
    {
      QSF_ASSERT(0 == 0, "debug function still in executable", throw);
      boost::nowide::ofstream ofs("C:\\Users\\Simon\\Desktop\\emx\\log.log");
      qsf::FileHelper::writeJson(ofs, tree);
    }

    //[-------------------------------------------------------]
    //[ Protected virtual QWidget methods                     ]
    //[-------------------------------------------------------]
    void JsonEditor::showEvent(QShowEvent* qShowEvent)
    {
      // Call the base implementation
      View::showEvent(qShowEvent);

      // Connect Qt signals/slots
      connect(&QSF_EDITOR_OPERATION, &qsf::editor::OperationManager::undoOperationExecuted, this, &JsonEditor::onUndoOperationExecuted);
      connect(&QSF_EDITOR_OPERATION, &qsf::editor::OperationManager::redoOperationExecuted, this, &JsonEditor::onRedoOperationExecuted);
    }

    void JsonEditor::hideEvent(QHideEvent* qHideEvent)
    { 
      // Call the base implementation
      View::hideEvent(qHideEvent);

      // Disconnect Qt signals/slots
      disconnect(&QSF_EDITOR_OPERATION, &qsf::editor::OperationManager::undoOperationExecuted, this, &JsonEditor::onUndoOperationExecuted);
      disconnect(&QSF_EDITOR_OPERATION, &qsf::editor::OperationManager::redoOperationExecuted, this, &JsonEditor::onRedoOperationExecuted);
    }

    //[-------------------------------------------------------]
    //[ Private Qt slots (MOC)                                ]
    //[-------------------------------------------------------]
    void JsonEditor::onPushButtonOpen(const bool pressed)
    {
      disconnect(mUiJsonEditor->dropdownFilePicker, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChangedFilePicker(int)));
      mUiJsonEditor->dropdownFilePicker->clear();
      // tr:
      QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Pick Unit Pool File"), "", tr("Unit Pool %1").arg("(*.json)"));

      for (int i = 0; i < fileNames.size(); i++)
      {
        mUiJsonEditor->dropdownFilePicker->addItem(fileNames[i]);
      }
      connect(mUiJsonEditor->dropdownFilePicker, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChangedFilePicker(int)));
      onCurrentIndexChangedFilePicker(mUiJsonEditor->dropdownFilePicker->currentIndex());     // manually send signal, that content of FilePicker changed
    }

    void JsonEditor::onPushButtonSave(const bool pressed)
    {
      // todo: save this part of the tree back to "parsedJson"
      std::string unitPoolName = mUiJsonEditor->dropdownUnitPool->currentText().toLocal8Bit().constData();
      std::string unitPoolPath = "UnitPool." + unitPoolName;

//      boost::property_tree::ptree::assoc_iterator iterator = parsedJson.find(unitPoolName);
      printPropertyTree(parsedJson);
      parsedJson.get_child("UnitPool").erase(unitPoolName);
//      parsedJson.get_child("UnitPool").add_child(unitPoolName, currentUnitPool);
    //  parsedJson.add_child("UnitPool.tutorial_unitpool", currentUnitPool);
//      parsedJson.add_child(std::string(unitPoolPath), boost::property_tree::ptree(currentUnitPool));

      boost::property_tree::ptree tempTree = currentUnitPool;
      boost::property_tree::ptree::assoc_iterator testIterator = parsedJson.get_child("UnitPool").find(unitPoolName);
      if (testIterator != parsedJson.get_child("UnitPool").not_found())
      {
        return;
      }

      parsedJson.add_child(unitPoolPath, tempTree);

      // write the entire Json file to disk
      boost::nowide::ofstream ofs(mUiJsonEditor->dropdownFilePicker->currentText().toLocal8Bit().constData());
      qsf::FileHelper::writeJson(ofs, parsedJson);

      // debug:
      int amountBOOST = 0;
      for (boost::property_tree::ptree::const_iterator iter = currentUnitPool.begin(); iter != currentUnitPool.end(); iter++)
      {
        amountBOOST++;
      }
      // debug:
      int amountQT = mUiJsonEditor->tableExistingUnitPool->rowCount();
      mUiJsonEditor->labelDebug->setText(QString::number(amountBOOST + 1) + QString::number(amountQT + 1));
    }

    void JsonEditor::onPushButtonSubmit(const bool pressed)
    {
      // QSF_LOG_PRINT(ERROR, "JsonEditor | onPushButtonSubmit");

      int newRowIndex = mUiJsonEditor->tableExistingUnitPool->rowCount();
      QString vehicleID = mUiJsonEditor->lineEditVehicleID->text();
      int amount = mUiJsonEditor->spinBoxAmount->value();

      if (vehicleID.length() == 0)
        return;

      if (amount < 1)
      { 
        mUiJsonEditor->spinBoxAmount->setValue(1);
        return;
      }

      // todo: check existence of key in currentUnitPool
      boost::property_tree::ptree::const_assoc_iterator tempIter = currentUnitPool.find(vehicleID.toLocal8Bit().constData());
      if(tempIter == currentUnitPool.not_found())     // key does not exist
      {
        mUiJsonEditor->tableExistingUnitPool->insertRow(newRowIndex);
        blockCellChange(true);
        mUiJsonEditor->tableExistingUnitPool->setItem(newRowIndex, 0, new QTableWidgetItem(vehicleID));
        blockCellChange(false);
        mUiJsonEditor->tableExistingUnitPool->setItem(newRowIndex, 1, new QTableWidgetItem(QString::number(amount)));
      }
      else      // key already exists
      {
        // find element in table to manipulate it
        QList<QTableWidgetItem*> tempList = mUiJsonEditor->tableExistingUnitPool->findItems(vehicleID, Qt::MatchFlag::MatchExactly);
        if (tempList.length() != 1)
        {
          // tr:
          QSF_LOG_PRINT(ERROR, QT_TR_NOOP("ID_EMX_JSON_TEST"));//"JsonEditor | onPushButtonSubmit: file is corrupted! keys are duplicated: "));//%1").arg(vehicleID).toLocal8Bit().constData());
          return;
        }
        // change value of specified key-value pair
        int row = tempList[0]->row();
        mUiJsonEditor->tableExistingUnitPool->item(row, 1)->setText(QString::number(amount));
      }

      mUiJsonEditor->lineEditVehicleID->clear();
      mUiJsonEditor->spinBoxAmount->clear();

      // todo: check whether the vehicleID is already available in the UnitPool
      // if yes, change its value
      // if no, add the key to the ptree
    }

    void JsonEditor::onCurrentIndexChangedFilePicker(int index)
    {
      // QSF_LOG_PRINT(ERROR, "JsonEditor | onCurrentIndexChangedFilePicker");

      QString filepath(mUiJsonEditor->dropdownFilePicker->itemText(index).toUtf8().constData());

      // load old jsonFile and unmount old UnitPool
      parsedJson.clear();
      currentUnitPool.clear();

      clearTable();

      mUiJsonEditor->dropdownUnitPool->clear();                 // remove all items
      mUiJsonEditor->pushButtonSubmit->setDisabled(true);       // block submit button
      mUiJsonEditor->pushButtonSave->setDisabled(true);         // block save button

      // todo: load jsonFile
      boost::nowide::ifstream ifs(filepath.toLocal8Bit().constData());
      //boost::nowide::ifstream ifs("C:\\Users\\Simon\\Desktop\\startup.json");
      qsf::FileHelper::readJson(ifs, parsedJson);

      // make sure it is of Format Type "em5_unitpool"
      // if not, remove the file entry from dropdown-menu and return
      {
        boost::optional<std::string> tempJsonFormatType = parsedJson.get_optional<std::string>("Format.Type");
        
        if(tempJsonFormatType.is_initialized() && tempJsonFormatType.get().compare("em5_unitpool") == 0)
        { }
        else
        {
          // tr:
          QSF_LOG_PRINTF(ERROR, tr("JsonEditor | onCurrentIndexChangedFilePicker: bad file: %1").arg(mUiJsonEditor->dropdownFilePicker->itemText(index)).toLocal8Bit().constData());
          mUiJsonEditor->dropdownFilePicker->removeItem(index);         // remove the not working file from dropdown
          parsedJson.clear();                                           // clear parsedJson-ptree
          mUiJsonEditor->pushButtonSubmit->setDisabled(false);          // unblock button
          // todo: load old json again?!
          return;
        }
      }

      boost::property_tree::ptree unitPoolNode;
      {
        boost::optional<boost::property_tree::basic_ptree<std::string, std::string, std::less<std::string>> &> unitPoolNodeOptional = parsedJson.get_child_optional("UnitPool");

        if (unitPoolNodeOptional.is_initialized())
        {
          unitPoolNode = unitPoolNodeOptional.get();
        }
        else
        {
          // tr:
          QSF_LOG_PRINTF(ERROR, tr("JsonEditor | onCurrentIndexChangedFilePicker: bad file: %1, could not find node \"UnitPool\"").arg(mUiJsonEditor->dropdownFilePicker->itemText(index)).toLocal8Bit().constData());
          return;
        }
      }

      // todo:
      //blockCellChange(true);

      for (boost::property_tree::ptree::const_iterator iter = unitPoolNode.begin(); iter != unitPoolNode.end(); iter++)
      {
        mUiJsonEditor->dropdownUnitPool->addItem(iter->first.c_str());
      }
      // todo:
      //blockCellChange(false);

      mUiJsonEditor->pushButtonSubmit->setDisabled(false);      // unblock submit button
      mUiJsonEditor->pushButtonSave->setDisabled(false);         // unblock save button
    }

    void JsonEditor::onCurrentIndexChangedUnitPool(int index)
    {
      // QSF_LOG_PRINT(ERROR, "JsonEditor | onCurrentIndexChangedUnitPool");

      QString unitPoolName(mUiJsonEditor->dropdownUnitPool->itemText(index).toUtf8().constData());

      if(unitPoolName.length() < 1)
      {
        // tr:
        QSF_LOG_PRINT(ERROR, tr("JsonEditor | onCurrentIndexChangedUnitPool: empty unitPoolName").toLocal8Bit().constData());
        return;
      }

      // todo: unmount old unitPool -> is this needed at all?
      currentUnitPool.clear();

      clearTable();
      mUiJsonEditor->pushButtonSubmit->setDisabled(true);       // block submit-button

      // todo: mount new one
//      boost::optional<boost::property_tree::basic_ptree<std::string, std::string, std::less<std::string>> &> unitPoolOptional = parsedJson.get_child("UnitPool").get_child_optional(unitPoolName.toLocal8Bit().constData());
      //std::string jsonPathToNewUnitPool = "UnitPool." + std::string(unitPoolName.toLocal8Bit().constData());
      boost::optional<boost::property_tree::basic_ptree<std::string, std::string, std::less<std::string>> &> unitPoolOptional = parsedJson.get_child_optional("UnitPool." + std::string(unitPoolName.toLocal8Bit().constData()));
      // todo:
      if (unitPoolOptional.is_initialized())
      { }
      else
      {
        // tr:
        QSF_LOG_PRINTF(ERROR, tr("JsonEditor | onCurrentIndexChangedUnitPool: specified UnitPool \"%1\" could not be found.").arg(unitPoolName).toLocal8Bit().constData());
        mUiJsonEditor->pushButtonSubmit->setDisabled(false);
        return;
      }

      currentUnitPool = unitPoolOptional.get();

      // debug:
      printPropertyTree(currentUnitPool);

      // todo:
      blockCellChange(true);
      int i = 0;
      for (boost::property_tree::ptree::const_iterator iter = currentUnitPool.begin(); iter != currentUnitPool.end(); iter++)
      {
        mUiJsonEditor->tableExistingUnitPool->insertRow(i);
        mUiJsonEditor->tableExistingUnitPool->setItem(i, 0, new QTableWidgetItem(iter->first.c_str()));
        mUiJsonEditor->tableExistingUnitPool->setItem(i, 1, new QTableWidgetItem(QString::number(std::stoi(iter->second.data()))));
        i++;
      }
      // debug:
      blockCellChange(false);

      // unblock button
      mUiJsonEditor->pushButtonSubmit->setDisabled(false);
    }

    void JsonEditor::onUnitPoolTableContentChanged(int row, int column)
    {
      //QSF_LOG_PRINT(ERROR, "JsonEditor | onUnitPoolTableContentChanged");

      QTableWidgetItem* currentItem = mUiJsonEditor->tableExistingUnitPool->item(row, column);

      // check if the string is not empty
      if (currentItem->text().length() == 0)
      {
        currentItem->setText(currentCellContent);
        return;
      }
      
      std::string unitPoolName = mUiJsonEditor->dropdownUnitPool->currentText().toLocal8Bit().constData();

      if (column == 0) // a key was changed
      {
        std::string newKey = mUiJsonEditor->tableExistingUnitPool->item(row, 0)->text().toLocal8Bit().constData();


        // check existence of key in currentUnitPool
        boost::property_tree::ptree::const_assoc_iterator tempIter = currentUnitPool.find(newKey);
        if (tempIter != currentUnitPool.not_found())     // key already exists
        {
          // tr:
          QSF_LOG_PRINTF(ERROR, tr("JsonEditor | onUnitPoolTableContentChanged: key already exists: \"%1\"").arg(currentCellContent).toLocal8Bit().constData());
          blockCellChange(true);
          currentItem->setText(currentCellContent);
          blockCellChange(false);
          return;
        }

        // if tempItem will be a nullptr, signals might have not been deactivated when rebuilding view's table
        QTableWidgetItem* tempItem = mUiJsonEditor->tableExistingUnitPool->item(row, 1);
        std::string amountContent = tempItem->text().toLocal8Bit().constData();
        std::string oldKey = currentCellContent.toLocal8Bit().constData();

        boost::property_tree::ptree::assoc_iterator iterator = currentUnitPool.find(oldKey);

        currentUnitPool.add_child(newKey, currentUnitPool.get_child(oldKey));
        //currentUnitPool.erase(oldKey);

        // todo:
        //iterator->second.push_front({ newKey, decltype(currentUnitPool)(amountContent) });
        //iterator->second.insert(currentUnitPool.to_iterator(iterator), {newKey, decltype(currentUnitPool)(amountContent)});
        currentUnitPool.erase(currentUnitPool.to_iterator(iterator));
        
      }
      else            // a value was changed
      {
        QString amountContentQString = mUiJsonEditor->tableExistingUnitPool->item(row, 1)->text();
        bool isInteger = false;
        int amount = amountContentQString.toInt(&isInteger);
        if (!isInteger)
        {
          mUiJsonEditor->tableExistingUnitPool->item(row, column)->setText(currentCellContent);
          // tr:
          QSF_LOG_PRINT(ERROR, tr("JsonEditor | onUnitPoolTableContentChanged: only numbers allowed!").toLocal8Bit().constData());
          return;
        }
        
        std::string keyContent = mUiJsonEditor->tableExistingUnitPool->item(row, 0)->text().toLocal8Bit().constData();
        std::string amountContent = mUiJsonEditor->tableExistingUnitPool->item(row, 1)->text().toLocal8Bit().constData();

        currentUnitPool.put(keyContent, amountContent);
      }
    }

    void JsonEditor::onUnitPoolTableCurrentItemChanged(QTableWidgetItem* current, QTableWidgetItem* previous)
    {
      // QSF_LOG_PRINT(ERROR, "JsonEditor | onUnitPoolTableCurrentItemChanged");
      if (current != nullptr)
      {
        currentCellContent = current->text();
      }
    }

    void JsonEditor::onUndoOperationExecuted(const qsf::editor::base::Operation& operation)
    {
      // QSF_LOG_PRINT(ERROR, "JsonEditor | onUndoOperationExecuted");
      // not (yet?) supported
    }

    void JsonEditor::onRedoOperationExecuted(const qsf::editor::base::Operation& operation)
    {
      // QSF_LOG_PRINT(ERROR, "JsonEditor | onRedoOperationExecuted");
      // not (yet?) supported
    }


    //[-------------------------------------------------------]
    //[ Namespace                                             ]
    //[-------------------------------------------------------]
  } // editor
} // emx
